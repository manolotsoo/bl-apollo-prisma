const { ApolloServer } = require('apollo-server')
const { typeDefs } = require('./schema')
const { resolvers } = require('./resolvers')
const { ApolloServerPluginLandingPageGraphQLPlayground } = require('apollo-server-core');

const port = process.env.PORT || 9090;

const server = new ApolloServer({ 
    resolvers, 
    typeDefs,
    plugins: [
        ApolloServerPluginLandingPageGraphQLPlayground({
          // options
        })
      ]
});

server.listen({ port }, () => console.log(`Server runs at: http://localhost:${port}`));